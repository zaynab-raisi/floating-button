import Vue from 'vue';
import App from './App.vue';
import router from './router';
import 'bootstrap/dist/css/bootstrap.min.css';

export const bus = new Vue();

export const counter = {
    addToCart() {
      bus.$emit("addToCart");
    }
}

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
